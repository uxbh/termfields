# Termfields

## Synopsis

A Go Library for creating and updating console based UIs. 

## Installation

```
go get -u gitlab.com/uxbh/termfields
```

## Contributors

Written by Unixblackhole


## License

See [License](LICENSE)

